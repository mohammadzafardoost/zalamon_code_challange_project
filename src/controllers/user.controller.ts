import validate from '../utils/validator.utility';
import bcrypt from '../utils/bcrypt.utility';
import jwt from '../utils/jwt.utility';
import random from '../utils/random.utility';
import { UserService } from '../services/user.service';

const userService = new UserService();

class UserController {
  async userLogin(email: string, password: string) {
    try {
      /* validate params */
      await validate.userLogin({ email, password });

      /* find duplicate user */
      const user = await userService.findByEmail(email);
      if (!user) {
        throw new Error('Email or password not valid!');
      }

      const { id: userId } = user;

      const comparePassword = await bcrypt.compareHash(password, user.password);
      if (!comparePassword) {
        throw new Error('Email or password not valid!');
      }

      const refreshTokenPayload = {
        type: 'REFRESH_TOKEN',
        id: userId,
      };

      const accessTokenPayload = {
        type: 'ACCESS_TOKEN',
        id: userId,
      };

      const refreshToken = jwt.sign(refreshTokenPayload, String(process.env.REFRESH_TOKEN_SECRET), Number(process.env.REFRESH_TOKEN_EXPIRE), String(process.env.ISS_NAME));
      const accessToken = jwt.sign(accessTokenPayload, String(process.env.ACCESS_TOKEN_SECRET), Number(process.env.ACCESS_TOKEN_EXPIRE), String(process.env.ISS_NAME));

      /* update */
      await userService.updateRefreshToken(userId, refreshToken);
      return { ...user._doc, refreshToken, accessToken };
    } catch (error) {
      throw error;
    }
  }

  async createUser(email: string, fullname: string, password: string) {
    try {
      /* validate params */
      await validate.createUser({ email, fullname, password });

      /* find duplicate user */
      const user = await userService.findByEmail(email);
      if (user) {
        throw new Error('Email has already been taken.');
      }

      /* generate id - hashing password */
      const id = `_USER_${random.int(16)}`;
      const hashedPassword = await bcrypt.generateHash(password);

      /* insert */
      return await userService.createUser(id, email, fullname, hashedPassword);
    } catch (error) {
      throw error;
    }
  }

  async getUsers() {
    try {
      /* get */
      return await userService.getUsers();
    } catch (error) {
      throw error;
    }
  }

  async getUserById(id: string) {
    try {
      /* validate params */
      await validate.getUserById({ id });

      /* find */
      const user = await userService.findById(id);
      if (!user) {
        throw new Error('User not found!');
      }
      return user;
    } catch (error) {
      throw error;
    }
  }

  async getUserByEmail(email: string) {
    try {
      /* validate params */
      await validate.getUserByEmail({ email });

      /* find */
      const user = await userService.findByEmail(email);
      if (!user) {
        throw new Error('User not found!');
      }
      return user;
    } catch (error) {
      throw error;
    }
  }

  async updateUser(id: string, email: NullableString, fullname: string, password: NullableString, tokenUserId: string) {
    try {
      /* validate params */
      await validate.updateUser({ id, email, fullname, password });

      /* The requesting token must match the ID you want to update */
      if (id !== tokenUserId) {
        throw new Error('You do not have permission to use this service!');
      }

      /* find user by id */
      const user = await userService.findById(id);
      if (!user) {
        throw new Error('User not found!');
      }

      if (email && email !== user.email) {
        const user = await userService.findByEmail(email);
        if (user) {
          if (user.id !== id) {
            throw new Error('Email has already been taken.');
          }
        }
      }

      let hashedPassword: NullableString;
      if (password) {
        /* hashing password */
        hashedPassword = await bcrypt.generateHash(password);
        userService.revokeRefreshToken(id);
      }

      /* update */
      return await userService.updateUserById(id, email, fullname, hashedPassword);
    } catch (error) {
      throw error;
    }
  }

  async deleteUser(id: string, tokenUserId: string) {
    try {
      /* validate params */
      await validate.deleteUser({ id });

      /* The requesting token must match the ID you want to update */
      if (id !== tokenUserId) {
        throw new Error('You do not have permission to use this service!');
      }

      /* find user by id */
      const user = await userService.findById(id);
      if (!user) {
        throw new Error('User not found!');
      }

      /* delete */
      return await userService.deleteUserById(id);
    } catch (error) {
      throw error;
    }
  }

  async getNewAccessToken(refreshToken: string) {
    try {
      /* validate params */
      await validate.getNewToken({ refreshToken });

      /* verify json web token [signature, expiration] */
      await jwt.verify(refreshToken, String(process.env.REFRESH_TOKEN_SECRET));

      /* find user by token */
      const user = await userService.findByRefreshToken(refreshToken);
      if (!user) {
        throw new Error('Token not valid');
      }

      const accessTokenPayload = {
        type: 'ACCESS_TOKEN',
        id: user.id,
      };

      /* sign token */
      const accessToken = jwt.sign(accessTokenPayload, String(process.env.ACCESS_TOKEN_SECRET), Number(process.env.ACCESS_TOKEN_EXPIRE), String(process.env.ISS_NAME));
      return { refreshToken, accessToken };
    } catch (error) {
      throw error;
    }
  }
}

export default UserController;
