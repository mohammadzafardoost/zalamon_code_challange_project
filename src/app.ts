import * as dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import cors from 'cors';
import { graphqlHTTP } from 'express-graphql';
import config from 'config';
import compression from 'compression';
import passport from 'passport';
import schema from './graphql/schema';
import log from './utils/log.utility';
import logger from './utils/logger.utility';
import ip from './utils/ip.utility';
import authentication from './middlewares/authentication.middleawre';

/* loaders */
import './loaders';

const app = express();

app
  .disable('x-powered-by')
  .use(express.json())
  .use(express.urlencoded({ extended: true }))
  .use(cors())
  .use(compression());

app.use(passport.initialize());
app.use(authentication.authenticate('jwt', { failWithError: true }), (err: any, req: any, res: any, next: any) => {
  /* remove authentication for introspection [graphql-playground] */
  try {
    if (req.body.query.trim().startsWith('query Introspection') && process.env.NODE_ENV === 'development') {
      req.isIntrospection = true;
      res.statusCode = 200;
      return next();
    }
  } catch {
    return next();
  }
  return next();
});

app.use(
  '/',
  graphqlHTTP((req: any) => ({
    schema,
    graphiql: process.env.NODE_ENV === 'development',
    context: req,
    customFormatErrorFn: (error) => {
      return { message: error.message };
    },
    extensions: ({ result, document, context }): any => {
      if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
        logger.graphql(result, document, context);
      }
    },
  })),
);

app.listen(config.get('server.port'), () => {
  log.success(`Node environment => ${process.env.NODE_ENV}`);
  log.success(`GraphQL server started => ${ip.currentAddress()}:${config.get('server.port')}`);
});
