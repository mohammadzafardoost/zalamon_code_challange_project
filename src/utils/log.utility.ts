import log from 'fancy-log';
import chalk from 'chalk';

class Log {
  constructor() {
    this.console = global.console.log;
  }

  console(message: any) {
    this.console(message);
  }

  normal(message: any) {
    log(message);
  }

  error(message: any) {
    const { redBright, black } = chalk;
    log(black.bgRed(' Error '), redBright(message));
  }

  warning(message: any) {
    const { yellow, black } = chalk;
    log(black.bgYellow(' Warning '), yellow(message));
  }

  info(message: any) {
    const { blueBright, black } = chalk;
    log(black.bgBlue(' Info '), blueBright(message));
  }

  logger(message: any) {
    const { yellow, black } = chalk;
    log(black.bgYellow(' logger '), yellow(message));
  }

  success(message: any) {
    const { greenBright, black } = chalk;
    log(black.bgGreen(' Success '), greenBright(message));
  }
}

export default new Log();
