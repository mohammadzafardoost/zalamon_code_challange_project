import randomize from 'randomatic';

class Random {
  int(count: number): number {
    try {
      return Number(randomize('0', count));
    } catch (error) {
      throw error;
    }
  }

  string(count: number): string {
    try {
      return randomize('Aa0', count);
    } catch (error) {
      throw error;
    }
  }
}

export default new Random();
