import bcrypt from 'bcrypt';

class Bcrypt {
  async generateHash(value: string) {
    try {
      const salt = await bcrypt.genSalt(8);
      const hash = await bcrypt.hash(value, salt);
      return hash;
    } catch (error) {
      throw error;
    }
  } 

  async compareHash(plainValue: string, hashedValue: string) {
    try {
      return await bcrypt.compare(plainValue, hashedValue);
    } catch (error) {
      throw error;
    }
  }
}

export default new Bcrypt();
