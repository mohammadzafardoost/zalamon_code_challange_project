import { DocumentNode, FormattedExecutionResult } from 'graphql';
import log from './log.utility';
import config from 'config';

class Logger {
  async graphql(result: FormattedExecutionResult, document: DocumentNode, context: any) {
    try {
      log.info(
        `*(${Intl.DateTimeFormat('en-US', config.get('dateOptions')).format(Date.now())})* - * (${context.method})* - * (${context.url})* - * (${
          context?.connection.remoteAddress
        })* `,
      );
      log.logger('-------------------Request Headers------------------');
      log.console('\n');
      log.console(context.headers);
      log.console('\n');
      log.logger('----------------------------------------------------');
      log.logger('--------------------Request Body--------------------');
      log.console('\n');
      log.console(context.body);
      log.console('\n');
      log.logger('--------------------------------------------------------');
      log.logger('--------------------Request Document--------------------');
      log.console('\n');
      log.console(document);
      log.console('\n');
      log.logger('------------------------------------------------------');
      log.logger('--------------------Request Result--------------------');
      log.console('\n');
      log.console(JSON.stringify(result, null, 2));
      log.console('\n');
      log.logger('----------------------------------------------------');
    } catch (error) {
      log.logger('--------------------Logging Error--------------------');
      log.console('\n');
      log.console(error);
      log.console('\n');
      log.logger('----------------------------------------------------');
    }
  }
}

export default new Logger();
