import jwt from 'jsonwebtoken';

class JWT {
  sign(payload: { type: string; id: string }, secret: string, expiresIn: number, issuer: string) {
    try {
      return jwt.sign(payload, secret, { expiresIn, issuer });
    } catch (error) {
      throw error;
    }
  }

  async verify(value: string, secret: string) {
    try {
      return jwt.verify(value, secret);
    } catch (error) {
      throw error;
    }
  }
}

export default new JWT();
