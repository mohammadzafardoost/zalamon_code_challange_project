import ip from 'ip';

class IP {
  currentAddress(): string {
    return ip.address();
  }
}

export default new IP();
