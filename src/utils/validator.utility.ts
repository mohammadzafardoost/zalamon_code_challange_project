import joi from '@hapi/joi';

export default {
  userLogin: async (...parameters: any) => {
    try {
      const schema = joi.object({
        email: joi.string().email().required(),
        password: joi.string().trim().min(6).required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  createUser: async (...parameters: any) => {
    try {
      const schema = joi.object({
        email: joi.string().email().required(),
        fullname: joi.string().trim().required(),
        password: joi.string().trim().min(6).required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  getUserById: async (...parameters: any) => {
    try {
      const schema = joi.object({
        id: joi.string().trim().required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  getUserByEmail: async (...parameters: any) => {
    try {
      const schema = joi.object({
        email: joi.string().email().required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  updateUser: async (...parameters: any) => {
    try {
      const schema = joi.object({
        id: joi.string().required(),
        email: joi.string().email(),
        fullname: joi.string().trim(),
        password: joi.string().trim().min(6),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  deleteUser: async (...parameters: any) => {
    try {
      const schema = joi.object({
        id: joi.string().required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },

  getNewToken: async (...parameters: any) => {
    try {
      const schema = joi.object({
        refreshToken: joi.string().required(),
      });
      await schema.validateAsync(parameters[0]);
    } catch (error) {
      throw error;
    }
  },
};
