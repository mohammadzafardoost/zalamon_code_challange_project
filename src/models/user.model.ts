import mongoose, { Schema, Document } from 'mongoose';

interface IUser extends Document {
  id: string;
  email: string;
  fullname: string;
  password: string;
  refreshToken: string;
  createdAt: number;
  modifiedAt: number;
}

const UserSchema: Schema = new Schema({
  id: { type: String },
  email: { type: String },
  fullname: { type: String },
  password: { type: String },
  refreshToken: { type: String },
  createdAt: { type: Number },
  modifiedAt: { type: Number },
});

UserSchema.pre<IUser>('save', function (next) {
  this.createdAt = new Date().getTime();
  this.modifiedAt = new Date().getTime();
  next();
});

UserSchema.index({ id: 1 }, { unique: true });
UserSchema.index({ email: 1 }, { unique: true });

export default mongoose.model<IUser>('User', UserSchema);
