import * as Passport from 'passport-jwt';
import passport from 'passport';

import { UserService } from '../services/user.service';

const userService = new UserService();

const opts = {
  jwtFromRequest: Passport.ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.ACCESS_TOKEN_SECRET,
};

passport.use(
  new Passport.Strategy(opts, async function (JwtPayload, done) {
    const result = await userService.findById(JwtPayload.id);
    if (!result) {
      return done(null, false);
    }
    return done(null, result);
  }),
);

passport.serializeUser(function (user: any, done) {
  done(null, user.id);
});

export default passport;
