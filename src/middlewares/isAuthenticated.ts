export const isAuthenticated = (context: any) => {
  if (!context?.user) {
    throw new Error('Unauthorized access!');
  }
};
