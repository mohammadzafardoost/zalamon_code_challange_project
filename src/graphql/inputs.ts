export default `
   input LoginInput {
    email: String!
    password: String!
   }

   input UserInput {
    email: String!
    fullname: String!
    password: String!
   }

   input UpdateUserInput {
    id: String!
    email: String
    fullname: String
    password: String
   }

   input DeleteUserInput {
    id: String!
   }

   input GenerateNewTokenInput {
    refreshToken: String!
   }
 `;
