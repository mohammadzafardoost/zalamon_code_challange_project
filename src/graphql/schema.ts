import { makeExecutableSchema } from 'graphql-tools';
import resolvers from './resolvers';
import inputs from './inputs';
import types from './types';
import queries from './queries';
import mutations from './mutations';

const typeDefs = `
  ${types}
  ${inputs}
  ${queries}
  ${mutations}
`;

export default makeExecutableSchema({ resolvers, typeDefs });
