export default `
  type Query {
    getUsers: [User],
    getUserById(id: String!): User,
    getUserByEmail(email: String!): User,
  }
`;
