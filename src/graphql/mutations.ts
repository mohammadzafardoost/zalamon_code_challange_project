export default `
  type Mutation {
    userLogin(input: LoginInput): UserLogin
    createUser(input: UserInput): User
    updateUser(input: UpdateUserInput): User
    deleteUser(input: DeleteUserInput): Result
    generateNewToken(input: GenerateNewTokenInput): Tokens
  }
`;
