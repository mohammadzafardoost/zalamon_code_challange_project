export default `
  type UserLogin {
    id: String
    email: String
    fullname: String
    refreshToken: String
    accessToken: String
    createdAt: String
    modifiedAt: String
  }

  type User {
    id: String
    email: String
    fullname: String
    createdAt: String
    modifiedAt: String
  }

  type Result {
    status: Boolean
  }

  type Tokens {
    refreshToken: String
    accessToken: String
  }
`;
