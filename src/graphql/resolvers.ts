import { RootFieldFilter } from 'graphql-tools';
import UserController from '../controllers/user.controller';
import { isAuthenticated } from '../middlewares/isAuthenticated';

const userController = new UserController();

export default {
  Query: {
    getUsers: async (root: RootFieldFilter, args: any, context: any) => {
      try {
        isAuthenticated(context);
        return await userController.getUsers();
      } catch (error) {
        throw error;
      }
    },
    getUserById: async (root: RootFieldFilter, { id }: any, context: any) => {
      try {
        isAuthenticated(context);
        return await userController.getUserById(id);
      } catch (error) {
        throw error;
      }
    },
    getUserByEmail: async (root: RootFieldFilter, { email }: any, context: any) => {
      try {
        isAuthenticated(context);
        return await userController.getUserByEmail(email);
      } catch (error) {
        throw error;
      }
    },
  },
  Mutation: {
    userLogin: async (root: RootFieldFilter, { input: { email, password } }: any, context: any) => {
      try {
        const result = await userController.userLogin(email, password);
        context.res.statusCode = 200;
        return result;
      } catch (error) {
        throw error;
      }
    },
    generateNewToken: async (root: RootFieldFilter, { input: { refreshToken } }: any, context: any) => {
      try {
        const result = await userController.getNewAccessToken(refreshToken);
        context.res.statusCode = 200;
        return result;
      } catch (error) {
        throw error;
      }
    },
    createUser: async (root: RootFieldFilter, { input: { email, fullname, password } }: any, context: any) => {
      try {
        const result = await userController.createUser(email, fullname, password);
        context.res.statusCode = 200;
        return result;
      } catch (error) {
        throw error;
      }
    },
    updateUser: async (root: RootFieldFilter, { input: { id, email, fullname, password } }: any, context: any) => {
      try {
        isAuthenticated(context);
        return await userController.updateUser(id, email, fullname, password, context.user.id);
      } catch (error) {
        throw error;
      }
    },
    deleteUser: async (root: RootFieldFilter, { input: { id } }: any, context: any) => {
      try {
        isAuthenticated(context);
        return await userController.deleteUser(id, context.user.id);
      } catch (error) {
        throw error;
      }
    },
  },
};
