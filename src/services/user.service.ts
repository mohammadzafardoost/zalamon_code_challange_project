import _ from 'lodash';
import UserModel from '../models/user.model';
import log from '../utils/log.utility';

class UserService {
  async createUser(id: string, email: string, fullname: string, password: string) {
    try {
      const user = new UserModel({ id, email, fullname, password });
      return await user.save();
    } catch (error) {
      log.error(`[user.service.createUser] Creating user failed with error: ${error}`);
      throw error;
    }
  }

  async findByEmail(email: string) {
    try {
      return await UserModel.findOne({ email });
    } catch (error) {
      log.error(`[user.service.findByEmail] Finding user failed with error: ${error}`);
      throw error;
    }
  }

  async findById(id: string) {
    try {
      return await UserModel.findOne({ id });
    } catch (error) {
      log.error(`[user.service.findById] Finding user failed with error: ${error}`);
      throw error;
    }
  }

  async findByRefreshToken(refreshToken: string) {
    try {
      return await UserModel.findOne({ refreshToken });
    } catch (error) {
      log.error(`[user.service.findByRefreshToken] Finding user failed with error: ${error}`);
      throw error;
    }
  }

  async getUsers() {
    try {
      return await UserModel.find();
    } catch (error) {
      log.error(`[user.service.getAllUsers] Getting users failed with error: ${error}`);
      throw error;
    }
  }

  async updateUserById(id: string, email: NullableString, fullname: NullableString, password: NullableString) {
    try {
      return await UserModel.findOneAndUpdate({ id }, _.pickBy({ email, fullname, password, modifiedAt: new Date().getTime() }, _.identity), { new: true });
    } catch (error) {
      log.error(`[user.service.updateUserById] Updating user failed with error: ${error}`);
      throw error;
    }
  }

  async deleteUserById(id: string) {
    try {
      const result = await UserModel.deleteOne({ id });
      if (result.ok !== 1 || result.n !== 1) {
        throw new Error('failed');
      }
      return { status: true };
    } catch (error) {
      log.error(`[user.service.deleteUserById] Deleting user failed with error: ${error}`);
      throw error;
    }
  }

  async revokeRefreshToken(id: string) {
    try {
      return await UserModel.findOneAndUpdate({ id }, { refreshToken: undefined }, { new: true });
    } catch (error) {
      log.error(`[user.service.revokeRefreshToken] Revoking user token failed with error: ${error}`);
      throw error;
    }
  }

  async updateRefreshToken(id: string, refreshToken: string) {
    try {
      if (!refreshToken) {
        throw new Error('Refresh Token not valid');
      }
      const result = await UserModel.findOneAndUpdate({ id }, { refreshToken, modifiedAt: new Date().getTime() }, { new: true });
      if (result.refreshToken !== refreshToken) {
        throw new Error('failed');
      }
      return { status: true };
    } catch (error) {
      log.error(`[user.service.updateRefreshToken] Updating user refresh token failed with error: ${error}`);
      throw error;
    }
  }
}

export { UserService };
