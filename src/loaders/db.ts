import mongoose from 'mongoose';
import config from 'config';
import log from '../utils/log.utility';

class mongooseHandler {
  dbName: string | undefined;
  username: string | undefined;
  password: string | undefined;
  host: string | undefined;
  port: string | undefined;

  constructor() {
    if (config.get('db.mongodb.status')) {
      log.info(`Mongoose starting...`);
      this.dbName = config.get('db.mongodb.dbName');
      this.username = config.get('db.mongodb.connection.username');
      this.password = config.get('db.mongodb.connection.password');
      this.host = config.get('db.mongodb.connection.host');
      this.port = config.get('db.mongodb.connection.port');
      this.mongooseStart();
    }
  }

  mongooseStart() {
    let connectionString: string;
    if (process.env.NODE_ENV === 'production') {
      connectionString = `mongodb+srv://${this.username}:${this.password}@${this.host}/${this.dbName}`;
    } else {
      connectionString =
        this.username == '' || this.password == ''
          ? `mongodb://${this.host}:${this.port}/${this.dbName}`
          : `mongodb://${this.username}:${this.password}@${this.host}:${this.port}/${this.dbName}`;
    }

    mongoose.connect(connectionString, config.get('db.mongodb.options'));

    mongoose.connection.on('connected', () => {
      log.success(`Mongoose started => ${this.host}:${this.port}`);
    });

    mongoose.connection.on('error', (err) => {
      log.error(`${err}`);
    });

    mongoose.connection.on('disconnected', () => {
      log.error(`Mongoose connection has closed.`);
      mongoose.disconnect();
    });

    mongoose.connection.on('reconnected', () => {
      log.success(`Mongoose reconnected successfuly [ host : ${this.host}, port: ${this.port} ]`);
    });
  }
}
const mongoHandler = new mongooseHandler();

export default mongoHandler;
