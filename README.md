# This is a ZALAMON code challenge Project.

--------------------------------------------------------

# Important: You must use yarn to install the dependencies

--------------------------------------------------------

## Please follow these steps for Development mode:

### `yarn`

### Rename ".env.dist" and set "NODE_ENV=development "

### In the config folder, rename "development.json.dist" to "development.json" and enter your MongoDb config

### `yarn run dev`

Open [http://localhost:8080](http://localhost:8080) to access Graphql Playground


## And follow these steps for Production mode:

### `yarn`

### Rename ".env.dist" and set "NODE_ENV=production "

### In the config folder, rename "production.json.dist" to "production.json" and enter your MongoDb config or use the current connection string (This connection string is active and usable)

### `yarn run build`

### `yarn start`

Open [http://localhost:8080](http://localhost:8080) to access Graphql


--------------------------------------------------------

## NOTE: All services are Secure and you can only use "userLogin" without Authentication, so Bearer Token must be set in all requests.

## NOTE: Playground is only accessible in Development mode.

--------------------------------------------------------